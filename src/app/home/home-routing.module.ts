import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';

const routes: Routes = [
  { path: '', redirectTo: 'ropa/listar', pathMatch: 'full' },
  {
    path: 'usuario',
    component: HomeComponent,
    loadChildren: () =>
      import('./usuario/usuario.module').then((module) => module.UsuarioModule),
  },
  {
    path: 'ropa',
    component: HomeComponent,
    loadChildren: () =>
      import('./ropa/ropa.module').then((module) => module.RopaModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
