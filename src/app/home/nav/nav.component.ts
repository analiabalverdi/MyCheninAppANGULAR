import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
 
  appTitulo: string = "MyCheninApp"

  constructor( private router: Router) { }

  ngOnInit(): void {
  }

  logout (){
    Swal.fire({
      title: 'Estás seguro de Cerrar Sesión?',
      showDenyButton: true,
      confirmButtonText: `Salir`,
      denyButtonText: `Cancelar`,
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire('Nos vemos!', '', 'success')        
        sessionStorage.clear(); 
        this.router.navigateByUrl('/auth');
      } 
    })
  
  }

}
