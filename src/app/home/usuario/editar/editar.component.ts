import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IUsuario } from '../../../shared/interfaces/IUsuario';
import { Router } from '@angular/router';
import { UsuarioServiceService } from '../../../shared/services/usuario-service.service';
import Swal from 'sweetalert2';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit, OnChanges {
  editarForm: FormGroup;
  params = { headers: { authorization: `` } };

  @Input() usuario: IUsuario;

  constructor(
    private formBuilder: FormBuilder,
    private editarService: UsuarioServiceService,
    private router: Router
  ) {
    this.editarForm = this.formBuilder.group({
      nombre: ['', [Validators.required]],
      apellido: ['', Validators.required],
      rol: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.editarForm.valueChanges.subscribe(
      (value) => {},
      (error) => console.log(error),
      () => console.log('Ok suscripción al Form')
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.usuario.currentValue) {
      this.editarForm.patchValue(changes.usuario.currentValue);
    }
  }

  editarUsuario(id: string) {
    const values = this.editarForm.value;
    this.setParams();
    this.editarService.editarUsuario(id, this.params, values).subscribe(
      (response) => {
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Ropa Editada con éxito!!!',
          showConfirmButton: false,
          timer: 1500,
        });
        this.router.navigateByUrl('/home/ropa/listar');
      },
      (error) => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Hubo un error en la Edición de Ropa',
        });
      }
    );
  }

  setParams() {
    this.params.headers.authorization = sessionStorage.getItem('token');
  }

  decodeToken() {
    const token = sessionStorage.getItem('token');
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(token);
    return decodedToken;
  }
}
