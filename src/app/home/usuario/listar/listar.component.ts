import { Component, OnInit } from '@angular/core';
import { UsuarioServiceService } from 'src/app/shared/services/usuario-service.service';
import { IUsuario } from '../../../shared/interfaces/IUsuario';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {

  usuarios: IUsuario[] = [];
  count = 0;
  params = { headers: { authorization: `` }, skip: 0, limit: 20 }
  usuarioSelected: IUsuario;


  constructor( private usuarioService: UsuarioServiceService, private router: Router ) { }

  ngOnInit(): void {
    this.getUsuario();
  }
  getUsuario(){
    this.setParams();
    this.usuarioService.listarUsuarios( this.params ).subscribe(
      response => {
        this.usuarios = response.data.resultado;
        this.count = response.count;
      },
      error => {
        console.log(error);
      },
    );
  }

  setParams(skip?:number, limit?:number) {

    if ( skip ) this.params.skip = skip;
    if ( limit ) this.params.limit = limit;
    this.params.headers.authorization = sessionStorage.getItem ('token');

  }

  
  editar(usuario: IUsuario) {
    this.usuarioSelected = usuario;
  }

  borrar( usuario: IUsuario ){
      this.setParams();
      this.usuarioService.borrarUsuario(usuario._id, this.params).subscribe(
        (response) => {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Usuario eliminado con éxito!!!',
            showConfirmButton: false,
            timer: 1500,
          });
          this.router.navigateByUrl('/home/ropa/listar');
        },
        (error) => {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Hubo un error en la Eliminación del Usuario',
          });
        }
      );
    }

  crearClick(): void {
    this.router.navigateByUrl('/home/usuario/crear');

  }

}
