import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import Swal from 'sweetalert2';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioServiceService } from '../../../shared/services/usuario-service.service';

@Component({
  selector: 'app-crear',
  templateUrl: './crear.component.html',
  styleUrls: ['./crear.component.css']
})
export class CrearComponent implements OnInit {
  
  crearForm: FormGroup;
  params = { headers: { authorization: `` }, }

  constructor(
    private formBuilder: FormBuilder,
    private crearService: UsuarioServiceService,
    private router: Router
  ) {
    this.crearForm = this.formBuilder.group({
      nombre: ['', [Validators.required]],
      apellido: ['', Validators.required],
      rol: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.crearForm.reset();
    this.crearForm.valueChanges.subscribe(
      value =>{},
      error => console.log(error),
       ()=> console.log("Ok suscripción al Form"),
    );  
  }

  crearUsuario() {
    const values = this.crearForm.value;
    this.setParams();
    this.crearService.crearUsuario(this.params,values).subscribe(
      (response) => {        
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Usuario creado con éxito!!!',
            showConfirmButton: false,
            timer: 1500
          });
          this.router.navigateByUrl('/home/usuario/listar');                    
      },
      (error) => {Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Hubo un error en la Creación del Usuario'
      })
      }
    );
  }

  setParams() {

    this.params.headers.authorization = sessionStorage.getItem ('token');

  }

  decodeToken() {
    const token = sessionStorage.getItem ('token');
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(token);
    return decodedToken;
  }
}
