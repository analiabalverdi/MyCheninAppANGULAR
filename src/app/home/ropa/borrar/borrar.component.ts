import { Component, OnInit } from '@angular/core';
import { RopaServiceService } from '../../../shared/services/ropa-service.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-borrar',
  templateUrl: './borrar.component.html',
  styleUrls: ['./borrar.component.css']
})
export class BorrarComponent implements OnInit {

  params = { headers: { authorization: `` }, }
  

  constructor(
    
    private borrarService: RopaServiceService,
    private router: Router) { }

  ngOnInit(): void {
  }

  borrarRopa(id: string) {
    this.setParams();
    this.borrarService.borrarRopa(id, this.params).subscribe(
      (response) => {        
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Se ha borrado la ropa correctamente!',
            showConfirmButton: false,
            timer: 1500
          });
          this.router.navigateByUrl('/home/ropa/listar');                    
      },
      (error) => {Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Hubo un error en la Eliminación de Ropa'
      })
      }
    );
  }

  setParams() {
    this.params.headers.authorization = sessionStorage.getItem ('token');
  }

  decodeToken() {
    const token = sessionStorage.getItem ('token');
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(token);
    return decodedToken;
  }
}
