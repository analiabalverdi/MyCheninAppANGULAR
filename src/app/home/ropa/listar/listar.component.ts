import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { IRopa } from 'src/app/shared/interfaces/IRopa';
import { RopaServiceService } from 'src/app/shared/services/ropa-service.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css'],
})
export class ListarComponent implements OnInit {

  ropa: IRopa[] = [];
  count = 0;
  params = { headers: { authorization: `` }, skip: 0, limit: 20 };

  ropaSelected: IRopa;

  constructor(
    private ropaService: RopaServiceService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getRopa();
  }

  getRopa() {
    this.setParams();
    this.ropaService.listarRopa(this.params).subscribe(
      (response) => {
        this.ropa = response.data.resultado;
        this.count = response.count;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  editar(ropa: IRopa) {
    this.ropaSelected = ropa;
  }

  borrar(ropa: IRopa) {
    this.setParams();
    this.ropaService.borrarRopa(ropa._id, this.params).subscribe(
      (response) => {
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Ropa eliminada con éxito!!!',
          showConfirmButton: false,
          timer: 1500,
        });
        this.router.navigateByUrl('/home/usuario/listar');
      },
      (error) => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Hubo un error en la Eliminación de Ropa',
        });
      }
    );
  }

  crearClick(): void {
    this.router.navigateByUrl('/home/ropa/crear');
  }

  setParams(skip?: number, limit?: number) {
    if (skip) this.params.skip = skip;
    if (limit) this.params.limit = limit;
    this.params.headers.authorization = sessionStorage.getItem('token');
  }
}
