import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import Swal from 'sweetalert2';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthLoginService } from '../../../shared/services/auth-login.service';
import { Router } from '@angular/router';
import { RopaServiceService } from '../../../shared/services/ropa-service.service';

@Component({
  selector: 'app-crear',
  templateUrl: './crear.component.html',
  styleUrls: ['./crear.component.css']
})
export class CrearComponent implements OnInit {

  crearForm: FormGroup;
  params = { headers: { authorization: `` }, }

  constructor(
    private formBuilder: FormBuilder,
    private crearService: RopaServiceService,
    private router: Router
  ) {
    this.crearForm = this.formBuilder.group({
      tipo: ['', [Validators.required]],
      cantidad: ['', Validators.required],
      precio: ['', Validators.required],
      descripcion: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.crearForm.reset();
    this.crearForm.valueChanges.subscribe(
      value =>{},
      error => console.log(error),
       ()=> console.log("Ok suscripción al Form"),
    );  
  }

  crearRopa() {
    const values = this.crearForm.value; 
    this.setParams();
    this.crearService.crearRopa(this.params, values).subscribe(
      (response) => {        
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Ropa creada con éxito!!!',
            showConfirmButton: false,
            timer: 1500
          });
          this.router.navigateByUrl('/home/ropa/listar');                    
      },
      (error) => {Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Hubo un error en la Creación de Ropa'
      })
      }
    );
  }

  setParams() {

    this.params.headers.authorization = sessionStorage.getItem ('token');

  }

  decodeToken() {
    const token = sessionStorage.getItem ('token');
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(token);
    return decodedToken;
  }
}
