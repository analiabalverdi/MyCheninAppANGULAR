import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChange,
  SimpleChanges,
} from '@angular/core';
import Swal from 'sweetalert2';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { RopaServiceService } from '../../../shared/services/ropa-service.service';
import { IRopa } from '../../../shared/interfaces/IRopa';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css'],
})
export class EditarComponent implements OnInit, OnChanges {
  editarForm: FormGroup;
  params = { headers: { authorization: `` } };

  @Input() ropa: IRopa;

  constructor(
    private formBuilder: FormBuilder,
    private editarService: RopaServiceService,
    private router: Router
  ) {
    this.editarForm = this.formBuilder.group({
      tipo: ['', [Validators.required]],
      cantidad: ['', Validators.required],
      precio: ['', Validators.required],
      descripcion: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.editarForm.valueChanges.subscribe(
      (value) => {},
      (error) => console.log(error),
      () => console.log('Ok suscripción al Form')
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.ropa.currentValue) {
      this.editarForm.patchValue(changes.ropa.currentValue);
    }
  }

  editarRopa(id: string) {
    const values = this.editarForm.value;
    this.setParams();
    this.editarService.editarRopa(id, this.params, values).subscribe(
      (response) => {
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Ropa Editada con éxito!!!',
          showConfirmButton: false,
          timer: 1500,
        });
        this.router.navigateByUrl('/home/usuario/listar');
      },
      (error) => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Hubo un error en la Edición de Ropa',
        });
      }
    );
  }

  setParams() {
    this.params.headers.authorization = sessionStorage.getItem('token');
  }

  decodeToken() {
    const token = sessionStorage.getItem('token');
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(token);
    return decodedToken;
  }
}
