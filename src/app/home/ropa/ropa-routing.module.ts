import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BorrarComponent } from './borrar/borrar.component';
import { CrearComponent } from './crear/crear.component';
import { EditarComponent } from './editar/editar.component';
import { ListarComponent } from './listar/listar.component';
import { RopaComponent } from './ropa.component';

const routes: Routes = [
  {path: '', component: RopaComponent},
  {path: 'crear', component: CrearComponent},
  {path: 'editar', component: EditarComponent},
  {path: 'listar', component: ListarComponent},
  {path: 'borrar', component: BorrarComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RopaRoutingModule { }
