import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RopaRoutingModule } from './ropa-routing.module';
import { CrearComponent } from './crear/crear.component';
import { ListarComponent } from './listar/listar.component';
import { EditarComponent } from './editar/editar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BorrarComponent } from './borrar/borrar.component';


@NgModule({
  declarations: [CrearComponent, ListarComponent, EditarComponent, BorrarComponent],
  imports: [
    CommonModule,
    RopaRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class RopaModule { }
