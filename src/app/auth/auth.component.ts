import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthLoginService } from '../shared/services/auth-login.service';
import { JwtHelperService } from "@auth0/angular-jwt";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
})
export class AuthComponent implements OnInit {
  
  authForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authLogin: AuthLoginService,
    private router: Router
  ) {
    this.authForm = this.formBuilder.group({
      mail: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  ngOnInit(): void {
  }

  login() {
    const values = this.authForm.value; 
    this.authLogin.login(values.mail, values.password).subscribe(
      (response) => {
        if (response.data.token) {
          const usuario = this.decodeToken();
         if(usuario.rol==='ADMIN'){
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Bienvenido a MyCheninApp!!!',
            showConfirmButton: false,
            timer: 1500
          })
            this.router.navigateByUrl('/home');
          }else{
            Swal.fire({ 
            icon: 'error',
            title: 'Oops...',
            text: 'No tenés permiso para acceder a la App!'
          })
            this.router.navigateByUrl('/auth');
          }                
        }
          
      },
      (error) => {Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Hubo un error de Usuario - Contraseña'
      })
      }
    );
  }

  decodeToken() {
    const token = sessionStorage.getItem ('token');
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(token);
    return decodedToken;
  }
}
