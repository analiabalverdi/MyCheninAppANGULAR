export interface IUsuario {
    _id?:string,
    nombre: string;
    apellido: string;
    password: string;
    email: string;
    rol: string;
}
