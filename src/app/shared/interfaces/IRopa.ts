export interface IRopa {
    _id?: string;
    tipo: string;
    cantidad: number;
    precio: number;
    descripcion: string;
}

