import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IUsuario } from '../interfaces/IUsuario';
import { IRopa } from '../interfaces/IRopa';

@Injectable({
  providedIn: 'root'
})
export class RopaServiceService {

  constructor( private httpClient: HttpClient ) {

   }

   listarRopa ( params: any): Observable <any>{
      const token = params.headers.authorization;
      const url = `http://localhost:3001/ropa`; 
      return this.httpClient.get <any>(url,{ headers: { authorization:`Bearer `+ token }});
   }

   crearRopa ( params: any, ropa: IRopa): Observable <any>{
      const token = params.headers.authorization;
      const httpOptions = {
         headers: new HttpHeaders({ authorization:`Bearer `+ token })
     };
      const url = `http://localhost:3001/ropa`; 
      return this.httpClient.post <any>(url, ropa , httpOptions);
   }

   editarRopa ( id:string, params: any, ropa: IRopa): Observable <any>{
      const token = params.headers.authorization;
      const httpOptions = {
         headers: new HttpHeaders({ authorization:`Bearer `+ token })
     };
      const url = `http://localhost:3001/ropa/`+id; 
      return this.httpClient.put <any>(url, ropa , httpOptions);
   }
    
   borrarRopa ( id: string, params: any): Observable <any>{
      const token = params.headers.authorization;
      const httpOptions = {
         headers: new HttpHeaders({ authorization:`Bearer `+ token })
     };
      const url = `http://localhost:3001/ropa/`+id; 
      return this.httpClient.delete <any>(url, httpOptions);
   }
}
