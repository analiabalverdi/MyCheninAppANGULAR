import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IUsuario } from '../interfaces/IUsuario';
import { tap } from 'rxjs/operators';
import { ILoginResponse } from '../interfaces/ILoginResponse';

@Injectable({
  providedIn: 'root',
})
export class AuthLoginService {

  usuario: IUsuario;

  constructor(private httpClient: HttpClient) {}

  login(email: string, password: string): Observable<any> {
    const url = `http://localhost:3001/auth/login`;
    return this.httpClient.post<ILoginResponse>(url, { email, password }).pipe( 
      tap((response) => {
         if (response.data.token){
           sessionStorage.setItem('token', response.data.token);                   
      }
     }));
  }

}
