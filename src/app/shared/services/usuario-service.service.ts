import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IUsuario } from '../interfaces/IUsuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioServiceService {

  constructor( private httpClient: HttpClient ) {

  }

  listarUsuarios ( params: any): Observable <any>{
     const token = params.headers.authorization;
     const url = `http://localhost:3001/usuario`; 
     return this.httpClient.get <any>(url,{ headers: { authorization:`Bearer `+ token }});
  }

  crearUsuario ( params: any, usuario: IUsuario): Observable <any>{
    const token = params.headers.authorization;
      const httpOptions = {
         headers: new HttpHeaders({ authorization:`Bearer `+ token })
     };
      const url = `http://localhost:3001/usuario/crear`; 
      return this.httpClient.post <any>(url, usuario , httpOptions);
 }
   editarUsuario ( id:string, params: any, usuario: IUsuario): Observable <any>{
    const token = params.headers.authorization;
    const httpOptions = {
     headers: new HttpHeaders({ authorization:`Bearer `+ token })
   };
    const url = `http://localhost:3001/usuario/`+id; 
    return this.httpClient.put <any>(url, usuario , httpOptions);
  }

   borrarUsuario ( id: string, params: any): Observable <any>{
    const token = params.headers.authorization;
    const httpOptions = {
     headers: new HttpHeaders({ authorization:`Bearer `+ token })
   };
    const url = `http://localhost:3001/usuario/`+id; 
    return this.httpClient.delete <any>(url, httpOptions);
  }

}
